#!/usr/bin/env bash
# if no node_modules folder is present - this script increase speed of npm i from 60 secs to 16

PKG_SUM=$(md5sum package-lock.json | cut -d\  -f 1)
NPM_TARBALL_CACHE=${HOME}/.cache/npmtarball
NPM_TARBALL=node_modules-${PKG_SUM}.tgz
NPM_TARBALL_FULL=${NPM_TARBALL_CACHE}/${NPM_TARBALL}
NPM_TARBALL_MD5SUM=${NPM_TARBALL_CACHE}/${NPM_TARBALL}.md5sum


[[ ! -e $NPM_TARBALL_CACHE ]] && mkdir -p $NPM_TARBALL_CACHE

function checkNpmMod() {
    md5sum -c ${NPM_TARBALL_MD5SUM} || rm -f ${NPM_TARBALL_FULL} ${NPM_TARBALL_MD5SUM}
    find ${NPM_TARBALL_CACHE} -mindepth 1 -mtime +14 -delete
    [[ -f ${NPM_TARBALL_FULL} ]] && tar xzf ${NPM_TARBALL_FULL}
}

function uploadNpmMod() {
    if [[ ! -f  ${NPM_TARBALL_FULL} ]];then
        if [[ -d node_modules ]];then
             tar zcf ${NPM_TARBALL_FULL} node_modules || return 1
        fi
        md5sum ${NPM_TARBALL_FULL} > ${NPM_TARBALL_MD5SUM}
    fi
}
checkNpmMod
npm set progress=false
npm install
uploadNpmMod