const webpack = require('webpack');

module.exports = {
  plugins: {
    precss: {},
    'postcss-cssnext': {},
    'postcss-font-magician': {},
    'postcss-color-rebeccapurple': {},
    'postcss-flexbugs-fixes': {},
    'postcss-custom-media': {},
    'postcss-media-minmax': {},
    'postcss-custom-properties': {},
    'postcss-import': { addDependencyTo: webpack }
  }
};
