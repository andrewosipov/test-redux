const path = require('path');
require('dotenv').config();

module.exports = {
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, './public/dist'),
    publicPath: '/dist/',
    libraryTarget: 'umd'
  },

  resolve: {
    modules: ['node_modules', './public/js/']
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: [{ loader: 'babel-loader' }]
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader',
            options: {
              modules: true,
              importLoaders: 1,
              localIdentName: '[folder]--[local]___[hash:base64:5]',
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              config: {
                path: './postcss.config.js'
              }
            }
          },
        ]
      },
      {
        test: /\.(png|gif|jpg|svg|ttf|eot|woff|woff2|ico)$/,
        use: {
          loader: 'url-loader',
          options: {
            name: '[name].[ext]',
            limit: 10,
            mimetype: 'image/svg+xml'
          }
        }
      }
    ]
  }
};
