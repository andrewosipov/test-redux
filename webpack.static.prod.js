const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const common = require('./webpack.config.js');
const Dotenv = require('dotenv-webpack');
const HandlebarsPlugin = require('handlebars-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = merge(common, {
  entry: {
    app: [
      './public/js/app.js'
    ]
  },
  mode: 'production',
  output: {
    filename: '[name].production.min.js',
    path: path.resolve(__dirname, './build/dist'),
    publicPath: '/dist/',
    libraryTarget: 'umd'
  },
  plugins: [
    new CopyWebpackPlugin([
      { from: './public/js/sw.js', to: path.resolve(__dirname, './build/js/sw.js') }
    ]),
    new HandlebarsPlugin({
      data: {
        isProduction: true,
        title: process.env.TITLE,
        timestamp: new Date().getTime()
      },
      entry: path.join(process.cwd(), 'views', 'main.hbs'),
      output: path.join(process.cwd(), 'build', 'index.html'),
      partials: [
        path.join(process.cwd(), 'views', '*.hbs')
      ],
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || ''),
      'process.env.LOGO_URL': JSON.stringify(process.env.LOGO_URL || ''),
      'process.env.PUBLIC_URL': JSON.stringify(process.env.PUBLIC_URL || ''),
      'process.env.API_URL': JSON.stringify(process.env.API_URL || ''),
      'process.env.CREDENTIALS': JSON.stringify(process.env.CREDENTIALS || ''),
      'process.env.WEB_PUSH_NOTIFICATION_KEY': JSON.stringify(process.env.WEB_PUSH_NOTIFICATION_KEY || ''),
      'process.env.TITLE': JSON.stringify(process.env.TITLE || '')
    }),
  ],
  optimization: {
    minimizer: [
      new UglifyJsPlugin()
    ]
  }
});
