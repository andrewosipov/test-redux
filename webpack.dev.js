const webpack = require('webpack');
const merge = require('webpack-merge');
const WebpackNotifierPlugin = require('webpack-notifier');

const common = require('./webpack.config.js');

module.exports = merge(common, {
  devtool: 'cheap-module-eval-source-map',
  entry: {
    app: [
      'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=200',
      'react-hot-loader/patch',
      './public/js/app.js'
    ]
  },
  mode: 'development',
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || ''),
      'process.env.LOGO_URL': JSON.stringify(process.env.LOGO_URL || ''),
      'process.env.PUBLIC_URL': JSON.stringify(process.env.PUBLIC_URL || ''),
      'process.env.API_URL': JSON.stringify(process.env.API_URL || ''),
      'process.env.CREDENTIALS': JSON.stringify(process.env.CREDENTIALS || ''),
      'process.env.WEB_PUSH_NOTIFICATION_KEY': JSON.stringify(process.env.WEB_PUSH_NOTIFICATION_KEY || ''),
      'process.env.TITLE': JSON.stringify(process.env.TITLE || '')
    }),
    new WebpackNotifierPlugin(),
    new webpack.HotModuleReplacementPlugin(),
  ],
});
