// @flow

import type { Hotel } from './Hotel';

export type Hotels = Array<Hotel>;
export type BaseReduxAction = { type: $Subtype<string> };

export type ResetAction = { type: 'RESET_HOTELS', payload: Hotels };
export type UpdateAction = { type: 'UPDATE_HOTELS', payload: Hotel };
export type AddAction = { type: 'ADD_HOTELS', payload: Allergen };
export type RemoveItemAction = { type: 'REMOVE_ITEM_HOTELS', payload: Allergen };

export type Action = ResetAction | UpdateAction | AddAction | RemoveItemAction | BaseReduxAction;
