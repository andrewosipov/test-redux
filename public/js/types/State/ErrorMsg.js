// @flow

export type ErrorMsg = {|
  +id?: string,
  +msg: string,
  +active?: boolean
|};
export type BaseReduxAction = { type: $Subtype<string> };

export type ResetAction = { type: 'RESET_ERROR', payload: ErrorMsg };
export type UpdateAction = { type: 'UPDATE_ERROR', payload: ErrorMsg };

export type Action = ResetAction | UpdateAction | BaseReduxAction;
