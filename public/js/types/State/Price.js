// @flow

export type Price = {|
  single: number,
  double: number,
  twin: number
|};
