// @flow

export type Routing = {|
  locationBeforeTransitions: Object
|};
