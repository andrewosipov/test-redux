// @flow

export type Base = {|
  errors: Object,
  isFetching: boolean
|};

export type BaseReduxAction = { type: $Subtype<string> };

export type ResetAction = { type: 'RESET_BASE', payload: Base };
export type UpdateAction = { type: 'UPDATE_BASE', payload: Base };

export type Action = ResetAction | UpdateAction | BaseReduxAction;
