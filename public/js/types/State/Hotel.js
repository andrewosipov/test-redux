// @flow

import type { Location } from './Location';
import type { Price } from './Price';

export type Hotel = {|
  id: ?number,
  name: string,
  img: string,
  address: string,
  location: ?Location,
  description: string,
  rate: number,
  hasPool: ?boolean,
  freebies: Array<string>,
  price: ?Price,
  errors?: Object,
  isFetching?: boolean
|};

export type BaseReduxAction = { type: $Subtype<string> };

export type ResetAction = { type: 'RESET_ALLERGEN', payload: Hotel };
export type UpdateAction = { type: 'UPDATE_ALLERGEN', payload: Hotel };

export type Action = ResetAction | UpdateAction | BaseReduxAction;
