// @flow

export type HotelsFilter = {|
  rate: ?number,
  hotelName: ?string,
  freebies: ?Array<string>
|};

export type BaseReduxAction = { type: $Subtype<string> };
export type ResetAction = { type: 'RESET_HOTELS_FILTER', payload: HotelsFilter };
export type UpdateAction = { type: 'UPDATE_HOTELS_FILTER', payload: HotelsFilter };

export type Action = ResetAction | UpdateAction | BaseReduxAction;
