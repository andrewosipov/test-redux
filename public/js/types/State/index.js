// @flow

import type { ErrorMsgs as _ErrorMsgs } from './ErrorMsgs';
import type { ErrorMsg as _ErrorMsg } from './ErrorMsg';
import type { Base as _Base } from './Base';
import type { Routing as _Routing } from './Routing';

import type { Price as _Price } from './Price';
import type { Hotel as _Hotel } from './Hotel';
import type { Hotels as _Hotels } from './Hotels';
import type { HotelsFilter as _HotelsFilter } from './HotelsFilter';

export type Price = _Price;
export type Hotel = _Hotel;
export type Hotels = _Hotels;
export type HotelsFilter = _HotelsFilter;

export type Routing = _Routing
export type ErrorMsgs = _ErrorMsgs;
export type ErrorMsg = _ErrorMsg;
export type Base = _Base;

export type State = {
  hotels: Hotels,
  hotel: Hotel,
  hotelsFilter: HotelsFilter,
  routing: Routing,
  base: Base,
  errors: ErrorMsgs
}
