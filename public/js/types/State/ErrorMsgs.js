// @flow

import type { ErrorMsg } from './ErrorMsg';

export type ErrorMsgs = Array<ErrorMsg>;
export type BaseReduxAction = { type: $Subtype<string> };
export type ResetAction = { type: 'RESET_ERRORS', payload: ErrorMsgs };
export type UpdateAction = { type: 'UPDATE_ERRORS', payload: ErrorMsg };
export type AddAction = { type: 'ADD_ERRORS', payload: ErrorMsg };
export type RemoveItemAction = { type: 'REMOVE_ITEM_ERRORS', payload: ErrorMsg };

export type Action = ResetAction | UpdateAction | AddAction | RemoveItemAction | BaseReduxAction;
