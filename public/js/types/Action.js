// @flow

import type { Action as ErrorMsgAction } from './State/ErrorMsg';
import type { Action as BaseAction } from './State/Base';
import type { Action as ErrorMsgsAction } from './State/ErrorMsgs';
import type { Action as HotelAction } from './State/Hotel';
import type { Action as HotelsAction } from './State/Hotels';
import type { Action as HotelsFilterAction } from './State/HotelsFilter';

export type BaseReduxAction = { type: $Subtype<string> };

export type Action =
  | HotelAction
  | HotelsAction
  | HotelsFilterAction
  | BaseAction
  | ErrorMsgAction
  | ErrorMsgsAction
  | BaseReduxAction
