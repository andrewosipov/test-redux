// @flow

import type {
  Store as ReduxStore,
  DispatchAPI,
} from 'redux';

import type { Action as _Action } from './Action';
import type { State as _State } from './State';

export type State = _State;
export type Action = _Action;
export type Dispatch = DispatchAPI<Action | ThunkAction>; // eslint-disable-line
export type Store = ReduxStore<State, Action, Dispatch>;
export type GetState = () => State;
export type PromiseAction = Promise<Action>;
export type ThunkAction = (dispatch: Dispatch, getState: GetState) => mixed;
