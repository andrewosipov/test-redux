import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

import { routerMiddleware } from 'react-router-redux';
import { browserHistory } from 'react-router';

import rootReducer from '../reducers/index';
// import createLogger from 'redux-logger'

const router = routerMiddleware(browserHistory);

export default function configureStore(initialState) {
  // const logger = createLogger()
  const store = createStore(
    rootReducer,
    initialState,
    compose(
      applyMiddleware(router, thunk), // logger
      window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__() : f => f
    )
  );

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      const nextRootReducer = require('../reducers/index');
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;
}
