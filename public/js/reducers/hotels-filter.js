// @flow

import {
  RESET_HOTELS_FILTER,
  UPDATE_HOTELS_FILTER
} from 'constants/hotels-filter';

import type { Action } from 'types/Store';
import type { HotelsFilter } from 'types/State';

const initialState = {
  rate: 0,
  hotelName: '',
  freebies: []
};

export default function (state: HotelsFilter = initialState, action: Action) {
  switch (action.type) {
    case RESET_HOTELS_FILTER:
      return { ...initialState, ...action.payload };
    case UPDATE_HOTELS_FILTER:
      return { ...state, ...action.payload };
    default:
      return state;
  }
}
