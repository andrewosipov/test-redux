import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import errors from './errors';
import base from './base';
import hotel from './hotel';
import hotels from './hotels';
import hotelsFilter from './hotels-filter';

export default combineReducers({
  hotelsFilter,
  hotels,
  hotel,
  base,
  errors,
  routing: routerReducer
});
