// @flow

import {
  RESET_HOTEL,
  UPDATE_HOTEL
} from 'constants/hotel';

import type { Action } from 'types/Store';
import type { Hotel } from 'types/State';

const initialState = {
  id: null,
  name: '',
  img: '',
  address: '',
  location: null,
  description: '',
  rate: 0,
  hasPool: null,
  price: null,
  freebies: [],
  errors: {},
  isFetching: false
};

export default function (state: Hotel = initialState, action: Action) {
  switch (action.type) {
    case RESET_HOTEL:
      return { ...initialState, ...action.payload };
    case UPDATE_HOTEL:
      return { ...state, ...action.payload };
    default:
      return state;
  }
}
