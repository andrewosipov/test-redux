// @flow

import {
  RESET_ERRORS,
  UPDATE_ERRORS,
  ADD_ERRORS,
  REMOVE_ITEM_ERRORS,
} from 'constants/errors';

import type { Action } from 'types/Store';
import type { ErrorMsgs } from 'types/State';

const initialState: ErrorMsgs = [];

export default function (state: ErrorMsgs = initialState, action: Action) {
  switch (action.type) {
    case RESET_ERRORS: {
      return action.payload ? [...action.payload] : [...initialState];
    }
    case UPDATE_ERRORS: {
      const payload = action.payload;
      const data = state.map((item) => {
        if (item.id !== payload.id) return { ...item };
        return { ...item, ...payload };
      });
      return [...data];
    }
    case REMOVE_ITEM_ERRORS: {
      const payload = action.payload;
      const data = state.reduce((container, item) => {
        if (item.id !== payload.id) {
          container.push(item);
        }
        return container;
      }, []);
      return [...data];
    }
    case ADD_ERRORS: {
      return [action.payload, ...state];
    }
    default: {
      return state;
    }
  }
}
