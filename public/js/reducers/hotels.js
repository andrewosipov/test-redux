// @flow

import {
  RESET_HOTELS,
  UPDATE_HOTELS,
  ADD_HOTELS,
  REMOVE_ITEM_HOTELS,
} from 'constants/hotels';

import type { Action } from 'types/Store';
import type { Hotels } from 'types/State';

const initialState: Hotels = [];

export default function (state: Hotels = initialState, action: Action) {
  switch (action.type) {
    case RESET_HOTELS: {
      return action.payload ? [...action.payload] : [...initialState];
    }
    case UPDATE_HOTELS: {
      const payload = action.payload;
      const data = state.map((item) => {
        if (item.id !== payload.id) return { ...item };
        return { ...item, ...payload };
      });
      return [...data];
    }
    case REMOVE_ITEM_HOTELS: {
      const payload = action.payload;
      const data = state.reduce((container, item) => {
        if (item.id !== payload.id) {
          container.push(item);
        }
        return container;
      }, []);
      return [...data];
    }
    case ADD_HOTELS: {
      return [action.payload, ...state];
    }
    default: {
      return state;
    }
  }
}
