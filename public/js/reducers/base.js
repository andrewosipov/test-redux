// @flow

import {
  RESET_BASE,
  UPDATE_BASE
} from 'constants/base';

import type { Action } from 'types/Store';
import type { Base } from 'types/State';


const initialState = {
  isFetching: false,
  errors: {}
};

export default function (state: Base = initialState, action: Action) {
  switch (action.type) {
    case RESET_BASE:
      return { ...initialState, ...action.payload };
    case UPDATE_BASE:
      return { ...state, ...action.payload };
    default:
      return state;
  }
}
