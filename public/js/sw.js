
self.addEventListener('push', (event) => {
  // console.log('[Service Worker] Push Received.');
  // console.log(event.data.text());
  // console.log(`[Service Worker] Push had this data: "${event.data.text()}"`);

  const data = event.data.json();
  const title = data.title;
  const options = data.options;

  event.waitUntil(self.registration.showNotification(title, options));
});

self.addEventListener('notificationclick', (event) => {
  const data = event.notification.data;
  event.notification.close();
  console.log(data);

  if (!event.action) {
    // Was a normal notification click
    console.log('Notification Click.');
    return;
  }

  switch (event.action) {
    case 'open_link':
      // event.waitUntil(clients.openWindow(data.orderLink));
      break;
    default:
      console.log(`Unknown action clicked: '${event.action}'`);
      break;
  }
});
