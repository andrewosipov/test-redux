// @flow

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { routerActions } from 'react-router-redux';

import Box from 'components/Box';
import Title from 'components/Title';
import Button from 'components/Button';
import Icon from 'components/Icon';
import Loader from 'components/Loader';
import Raiting from 'components/Raiting';

import * as baseActions from 'actions/base';
import * as hotelActions from 'actions/hotel';

import type { State as StoreState, Dispatch } from 'types/Store';
import type {
  Hotel,
  Hotels
} from 'types/State';

declare var _t: (str: string) => string;

type Props = {
  params: { id: string },
  routerActions: any,
  hotelsState: Hotels,
  hotelState: Hotel,
  hotelActions: typeof hotelActions
};

class HotelDetailPage extends Component<Props> {
  shouldComponentUpdate(nextProps) {
    return (
      this.props.hotelState.id !== nextProps.hotelState.id ||
      this.props.hotelState.isFetching !== nextProps.hotelState.isFetching ||
      this.props.hotelsState !== nextProps.hotelsState
    );
  }

  componentDidMount() {
    if (this.props.hotelsState.length > 0 && this.props.hotelState.id === null) {
      this.fetchData();
    }
  }

  componentDidUpdate() {
    if (this.props.hotelsState.length > 0 && this.props.hotelState.id === null) {
      this.fetchHotelData();
    }
  }

  fetchData() {
    if (this.props.hotelsState.length > 0 && this.props.hotelState.id === null) {
      this.fetchHotelData();
    }
  }

  async fetchHotelData() {
    const id = +this.props.params.id;
    const hotel = this.props.hotelsState.find((item) => {
      return (item.id === id);
    });

    if (hotel) {
      this.props.hotelActions.reset();
      this.props.hotelActions.update(hotel);
    }

    if (id) {
      await this.props.hotelActions.fetch({ id });
    }
  }

  render() {
    const { hotelState } = this.props;
    if (hotelState.id === null) {
      return null;
    }

    return (
      <Box mods={['stretch', 'column', 'full', 'relative', 'between']}>
        <Loader mods={[{ show: hotelState.isFetching }]} />
        <Box mods={['content', 'between', 'column', 'align-top']}>
          <Box mods={['action', 'indent-bottom-2']}>
            <Button
              icon={<Icon name="back" mods={['small', 'indent']} />}
              mods={['simple']}
              label={_t('Hotels')}
              onClick={this.handlebackClick}
            />
          </Box>
          <Box mods={['stretch', 'center', 'relative']}>
            <Box mods={['fourth']}>
              <img src={hotelState.img} alt={hotelState.name} />
            </Box>
            <Box mods={['three-fourth']}>
              <Box mods={['indent-bottom-2']}>
                <h1>{hotelState.name}</h1>
              </Box>
              <Box mods={['border', 'notify']}>
                {hotelState.price && `Single: $${hotelState.price.single} | Double: $${hotelState.price.double} | Twin: $${hotelState.price.twin}`}
              </Box>
              <Box mods={['indent-bottom-1', 'indent-top-1']}>
                {hotelState.address}
              </Box>
              <Box mods={['indent-bottom-1']}>
                <Raiting rate={hotelState.rate} />
              </Box>
              <Box mods={['indent-bottom-1']}>
                {hotelState.hasPool ? _t('Has pool') : null}
              </Box>
              <Box mods={['indent-bottom-1']}>
                {
                  hotelState.freebies
                    .map((item) => {
                      if (item === 'breakfast') return 'Free Breakfast';
                      if (item === 'parking') return 'Free Parking';
                      if (item === 'cancellation') return 'Free Cancellation';
                      if (item === 'shattle') return 'Free Airport Shattle';
                      return item;
                    })
                    .join(' | ')
                }
              </Box>
              <Box mods={['indent-bottom-1', 'column']}>
                <Title mods={['small']}>Description</Title>
                <Box mods={['indent-top-2']}>
                  {hotelState.description}
                </Box>
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
    );
  }

  handlebackClick = () => {
    this.props.routerActions.push('/hotels');
  }
}

const mapStateToProps = ((state: StoreState) => {
  return {
    hotelState: state.hotel,
    hotelsState: state.hotels
  };
});

const mapDispatchToProps = ((dispatch: Dispatch) => ({
  hotelActions: bindActionCreators(hotelActions, dispatch),
  baseActions: bindActionCreators(baseActions, dispatch),
  routerActions: bindActionCreators(routerActions, dispatch),
}));

export default connect(mapStateToProps, mapDispatchToProps)(HotelDetailPage);
