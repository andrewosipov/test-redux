import React, { Component, Fragment } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { routerActions } from 'react-router-redux';

import * as errorsActions from 'actions/errors';
import * as baseActions from 'actions/base';

import Svg from 'components/Svg';
import Layout from 'components/Layout';

import type { State as StoreState, Dispatch } from 'types/Store';

type Props = {
  children?: any,
  baseActions: typeof baseActions
}

let isFetched = false;

class AppPage extends Component<Props> {
  componentDidMount() {
    !isFetched && this.fetchAllData();
    isFetched = true;
  }

  async fetchAllData() {
    await this.props.baseActions.fetchAllData();
  }

  render() {
    return (
      <Layout mods={['fixed']}>
        <Fragment>
          <Svg />
          {this.props.children}
        </Fragment>
      </Layout>
    );
  }
}

function mapStateToProps(state: StoreState) {
  return {
    baseState: state.base,
    errorsStete: state.errors
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    routerActions: bindActionCreators(routerActions, dispatch),
    baseActions: bindActionCreators(baseActions, dispatch),
    errorsActions: bindActionCreators(errorsActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AppPage);
