// @flow

import { createSelector } from 'reselect';
import type { State as StoreState } from 'types/Store';
import type { Hotels, HotelsFilter } from 'types/State';

const getHotelsState = (state: StoreState): Hotels => state.hotels;
const getFilterState = (state: StoreState): HotelsFilter => state.hotelsFilter;

export const getFiltredHotels = createSelector(
  [getHotelsState, getFilterState],
  (list, params) => {
    return list
      .filter((item) => {
        if (params.hotelName && params.hotelName !== '') {
          return item.name.match(new RegExp(params.hotelName, 'im'));
        }
        return true;
      })
      .filter((item) => {
        if (params.rate && params.rate > 0 && item.rate) {
          return item.rate >= params.rate;
        }
        return true;
      })
      .filter((item) => {
        if (params.freebies && params.freebies.length > 0) {
          return item.freebies.some(f => params.freebies.some(p => p === f));
        }
        return true;
      });
  }
);

