// @flow

import React, { Component } from 'react';
import Box from 'components/Box';
import Title from 'components/Title';
import Button from 'components/Button';
import Raiting from 'components/Raiting';

import type {
  Price
} from 'types/State';

declare var _t: (str: string) => string;

type Props = {
  name: string,
  img: string,
  address: string,
  price: Price,
  rate: number,
  freebies: Array<string>,
  routerAction: Function
};

export default class HotelItem extends Component<Props> {
  render() {
    const { name, img, address, price, rate, freebies, routerAction } = this.props;
    return (
      <Box mods={['stretch', 'indent-bottom-3', 'height-230']}>
        <Box mods={['image-cover']}>
          <img src={img} alt={name} />
        </Box>
        <Box mods={['three-fourth', 'indent-left-2', 'border-bottom']}>
          <Box mods={['three-fourth']}>
            <Box mods={['indent-bottom-2', 'indent-right-2']}>
              <Title>{name}</Title>
            </Box>
            <Box mods={['indent-bottom-1']}>
              {address}
            </Box>
            <Box mods={['indent-bottom-1']}>
              <Raiting rate={rate} />
            </Box>
            <Box mods={['indent-bottom-1']}>
              {
                freebies
                  .map((item) => {
                    if (item === 'breakfast') return 'Free Breakfast';
                    if (item === 'parking') return 'Free Parking';
                    if (item === 'cancellation') return 'Free Cancellation';
                    if (item === 'shattle') return 'Free Airport Shattle';
                    return item;
                  })
                  .join(' | ')
              }
            </Box>
          </Box>
          <Box mods={['fourth', 'border-left', 'indent-2', 'indent-bottom-1', 'column']}>
            <Box mods={['grow-1', 'align-center', 'center']}>
              ${price.single}
            </Box>
            <Box mods={['center']}>
              <Button
                mods={['red']}
                label={_t('View Deal')}
                onClick={() => routerAction()}
              />
            </Box>
          </Box>
        </Box>
      </Box>
    );
  }
}
