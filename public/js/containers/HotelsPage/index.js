// @flow

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { routerActions } from 'react-router-redux';

import Box from 'components/Box';
import Filter from 'components/Filter';
import Loader from 'components/Loader';

import isEmpty from 'lodash/isEmpty';

import * as hotelsFilterActions from 'actions/hotels-filter';
import * as hotelActions from 'actions/hotel';
import * as hotelsActions from 'actions/hotels';
import * as baseActions from 'actions/base';

import type { State as StoreState, Dispatch } from 'types/Store';
import type {
  Hotels,
  Hotel,
  HotelsFilter,
  Base
} from 'types/State';

import HotelItem from './HotelItem';
import * as selectors from './selectors';

declare var _t: (str: string) => string;

type Props = {
  routerActions: typeof routerActions,
  hotelsState: Hotels,
  hotelState: Hotel,
  baseState: Base,
  hotelsFilterState: HotelsFilter,
  hotelsFilterActions: typeof hotelsFilterActions,
  baseActions: typeof baseActions
}

class HotelsPage extends Component<Props> {
  renderList() {
    const { hotelsState } = this.props;
    if (isEmpty(hotelsState)) {
      return (
        <Box mods={['center', 'stretch', 'align-center']}>
          {_t('Not Found')}
        </Box>
      );
    }
    return hotelsState.map(hotel => (
      <HotelItem
        key={hotel.id}
        routerAction={() => this.props.routerActions.push(`/hotels/${hotel.id}`)}
        {...hotel}
      />
    ));
  }

  render() {
    return (
      <Box mods={['stretch', 'full', 'relative', 'between']}>
        <Box mods={['fourth', 'indent-right-4']}>
          <Filter
            items={['stars', 'freebies', 'hotelName']}
            baseState={this.props.baseState}
            hotelsFilterState={this.props.hotelsFilterState}
            hotelsFilterActions={this.props.hotelsFilterActions}
            baseActions={this.props.baseActions}
          />
        </Box>
        <Box mods={['three-fourth', 'expand', 'scrollable']}>
          <Box mods={['grow-1', 'wrap', 'align-start', 'relative']}>
            <Loader mods={[{ show: this.props.baseState.isFetching }]} />
            {this.renderList()}
          </Box>
        </Box>
      </Box>
    );
  }
}

const mapStateToProps = ((state: StoreState) => {
  const hotelsState = selectors.getFiltredHotels(state);
  const baseState = state.base;
  return {
    baseState,
    hotelsState,
    hotelsFilterState: state.hotelsFilter
  };
});

const mapDispatchToProps = ((dispatch: Dispatch) => ({
  hotelsFilterActions: bindActionCreators(hotelsFilterActions, dispatch),
  hotelActions: bindActionCreators(hotelActions, dispatch),
  hotelsActions: bindActionCreators(hotelsActions, dispatch),
  baseActions: bindActionCreators(baseActions, dispatch),
  routerActions: bindActionCreators(routerActions, dispatch)
}));

export default connect(mapStateToProps, mapDispatchToProps)(HotelsPage);
