// @flow

export const RESET_HOTELS_FILTER = 'RESET_HOTELS_FILTER';
export const UPDATE_HOTELS_FILTER = 'UPDATE_HOTELS_FILTER';
