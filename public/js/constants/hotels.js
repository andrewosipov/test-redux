// @flow

export const RESET_HOTELS = 'RESET_HOTELS';
export const UPDATE_HOTELS = 'UPDATE_HOTELS';
export const ADD_HOTELS = 'ADD_HOTELS';
export const REMOVE_ITEM_HOTELS = 'REMOVE_ITEM_HOTELS';
