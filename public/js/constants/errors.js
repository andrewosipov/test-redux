// @flow

export const RESET_ERRORS = 'RESET_ERRORS';
export const UPDATE_ERRORS = 'UPDATE_ERRORS';
export const ADD_ERRORS = 'ADD_ERRORS';
export const REMOVE_ITEM_ERRORS = 'REMOVE_ITEM_ERRORS';
