export default {
  hostname: process.env.API_URL,
  apiToken: process.env.CREDENTIALS,
  pushKey: process.env.WEB_PUSH_NOTIFICATION_KEY,
  api: {
    hotels: {
      path: '/api/hotels'
    },
  }
};
