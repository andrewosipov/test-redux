function requireAll(requireContext) {
  return requireContext.keys().reduce((container, item) => {
    const fileName = item.replace(/(.*)\/(.*)\.(.*)/, '$2');
    container.push({
      name: fileName,
      svg: requireContext(item)
    });
    return container;
  }, []);
}

export default requireAll(require.context('!raw-loader?name=[path][name].[ext]!../img/svg', true, /\.svg$/));
