export const REFRESH_INTERVAL = 15 * 1000; // 15 seconds

export const allergens = [
  {
    name: 'Celery',
    value: 'celery',
    icon: 'allergen-celery'
  },
  {
    name: 'Crustaceans',
    value: 'crustaceans',
    icon: 'allergen-crus'
  },
  {
    name: 'Eggs',
    value: 'eggs',
    icon: 'allergen-egg'
  },
  {
    name: 'Fish',
    value: 'fish',
    icon: 'allergen-fish'
  },
  {
    name: 'Gluten',
    value: 'gluten',
    icon: 'allergen-gluten'
  },
  {
    name: 'Lupin',
    value: 'lupin',
    icon: 'allergen-lupin'
  },
  {
    name: 'Milk',
    value: 'milk',
    icon: 'allergen-milk'
  },
  {
    name: 'Molluscs',
    value: 'molluscs',
    icon: 'allergen-mollusc'
  },
  {
    name: 'Mustard',
    value: 'mustard',
    icon: 'allergen-mustard'
  },
  {
    name: 'Nuts',
    value: 'nuts',
    icon: 'allergen-nuts'
  },
  {
    name: 'Peanuts',
    value: 'peanuts',
    icon: 'allergen-peanuts'
  },
  {
    name: 'Sesame Seeds',
    value: 'sesame_seeds',
    icon: 'allergen-sesame'
  },
  {
    name: 'Soya',
    value: 'soya',
    icon: 'allergen-soya'
  },
  {
    name: 'Sulfur dioxide',
    value: 'sulfur_dioxide',
    icon: 'allergen-dioxide'
  }
];

export const MINUTES_PER_HOUR = 60;

export const timeSlots = {
  private: {
    timeStep: 15,
    fromTime: '00:00',
    toTime: '23:45'
  },

  nhs: {
    timeStep: 30,
    fromTime: '11:00',
    toTime: '18:30'
  }
};

export const recipientTypes = {
  patient: 'Patient',
  visitor: 'Visitor',
  defaultType: 'patient'
};

export const orderStatuses = {
  c: 'completed',
  n: 'new',
  in_progress: 'in progress',
  cancelled: 'cancelled'
};

export const orderColorStatuses = {
  c: 'primary',
  n: 'new',
  in_progress: 'progress',
  cancelled: 'warning'
};

export const userLevels = [
  {
    id: 'ROLE_USER',
    name: 'User'
  },
  {
    id: 'ROLE_SYSTEM_ADMIN',
    name: 'System Administrator'
  },
  {
    id: 'ROLE_ORGANIZATION_ADMIN',
    name: 'Organization Administrator'
  },
  {
    id: 'ROLE_WARD_HOSTESS',
    name: 'Ward Hostess'
  },
  {
    id: 'ROLE_PICKER',
    name: 'Picker'
  },
];

export const dietaries = [
  {
    name: 'Hallal',
    value: 'Hallal',
    icon: 'tag-halal'
  },
  {
    name: 'Kosher',
    value: 'kosher',
    icon: 'tag-ksher'
  },
  {
    name: 'Vegan',
    value: 'vegan',
    icon: 'tag-vegan'
  },
  {
    name: 'Vegetarian',
    value: 'vegetarian',
    icon: 'tag-vegetarian'
  }
];

export const patientTypes = [
  { name: 'Inpatient', value: 'i' },
  { name: 'Daypatient', value: 'd' }
];
