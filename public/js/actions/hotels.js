// @flow

import {
  RESET_HOTELS,
  UPDATE_HOTELS,
  ADD_HOTELS,
  REMOVE_ITEM_HOTELS
} from 'constants/hotels';
import RestProvider from 'providers/Rest';
import * as errorsActions from 'actions/errors';
import config from 'config';

import type { Dispatch, Action } from 'types/Store';
import type { Hotels, Hotel } from 'types/State';

declare var _t: (str: string) => string;

const path = config.api.hotels.path;
const provider = new RestProvider({ path });

export const reset = (payload: Hotels): Action => ({ type: RESET_HOTELS, payload });
export const update = (payload: $Shape<Hotel>): Action => ({ type: UPDATE_HOTELS, payload });
export const add = (payload: Hotel): Action => ({ type: ADD_HOTELS, payload });
export const removeItem = (payload: Hotel): Action => ({ type: REMOVE_ITEM_HOTELS, payload });

export function fetch(params?: Object) {
  return async (dispatch: Dispatch) => {
    try {
      const { status, data } = await provider.fetch(params);
      if (status === 200) {
        dispatch(reset(data));
        return data;
      }
      return null;
    } catch (e) {
      console.error(e);
      dispatch(errorsActions.push({ msg: _t('Server sync error') }));
      return null;
    }
  };
}

export function save(params: Object) {
  return async (dispatch: Dispatch) => {
    try {
      const { status, data } = await provider.save(params);
      if (status === 200) {
        dispatch(reset(data));
        return data;
      }
    } catch (e) {
      console.error(e);
      dispatch(errorsActions.push({ msg: _t('Server sync error') }));
      dispatch(update({ ...params, isFetching: false }));
    }
    return [];
  };
}
