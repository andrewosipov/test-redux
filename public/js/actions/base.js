// @flow

import {
  RESET_BASE,
  UPDATE_BASE
} from 'constants/base';

import * as errorsActions from 'actions/errors';
import * as hotelsActions from 'actions/hotels';

import type { Dispatch, Action } from 'types/Store';
import type { Base } from 'types/State';

declare var _t: (str: string) => string;

export const reset = (payload: $Shape<Base>): Action => ({ type: RESET_BASE, payload });
export const update = (payload: $Shape<Base>): Action => ({ type: UPDATE_BASE, payload });

export const fetchAllData = () => {
  return async (dispatch: Dispatch) => {
    dispatch(update({ isFetching: true }));
    try {
      await dispatch(hotelsActions.fetch());
      dispatch(update({ isFetching: false }));

      return null;
    } catch (e) {
      console.error(e);
      dispatch(errorsActions.push({ msg: _t('Server sync error') }));
      dispatch(update({ isFetching: false }));

      return null;
    }
  };
};
