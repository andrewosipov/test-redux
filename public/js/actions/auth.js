// @flow

import {
  RESET_AUTH,
  UPDATE_AUTH
} from 'constants/auth';

import RestProvider from 'providers/Rest';
import * as errorsActions from 'actions/errors';
import { deleteCookieByName, setCookie } from 'utils';

import config from 'config';

import type { Dispatch, Action } from 'types/Store';
import type { Auth } from 'types/State';
import decode from 'jwt-decode';

const tokenPath = config.api.token.path;
const provider = new RestProvider({ tokenPath });

declare var _t: (str: string) => string;

export const reset = (payload: $Shape<Auth>): Action => ({ type: RESET_AUTH, payload });
export const update = (payload: $Shape<Auth>): Action => ({ type: UPDATE_AUTH, payload });


export const logout = () => {
  return () => {
    deleteCookieByName('token');
    deleteCookieByName('refresh_token');
    window.location = '/auth';
  };
};

export const auth = (params: $Shape<Auth>) => {
  return async (dispatch: Dispatch) => {
    dispatch(update({ isFetching: true }));
    try {
      const { status, data } = await provider.api({
        method: 'POST',
        url: tokenPath,
        data: params
      });

      if (status === 200) {
        if (data.invitation_id && data.accept_url) window.location = data.accept_url;
        const tokenData = decode(data.token);
        setCookie('token', data.token, { path: '/', expires: 3600 });
        setCookie('refresh_token', data.refresh_token, { path: '/', expires: 3600 * 24 * 30 });
        dispatch(reset({ ...data, ...tokenData }));
        return data;
      }

      if (status === 401) {
        const errors = { username: 'invalid', password: 'invalid' };
        dispatch(update({ errors, isFetching: false }));
        return { errors };
      }

      if (status === 422) {
        dispatch(update({ errors: data.errors, isFetching: false }));
        return { errors: data.errors };
      }
      dispatch(update({ isFetching: false }));
      return null;
    } catch (e) {
      console.error(e);
      dispatch(update({ isFetching: false }));
      dispatch(errorsActions.push({ msg: _t('Server sync error') }));
    }
    return null;
  };
};
