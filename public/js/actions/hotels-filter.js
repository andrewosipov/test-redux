// @flow

import {
  RESET_HOTELS_FILTER,
  UPDATE_HOTELS_FILTER
} from 'constants/hotels-filter';

import type { Action } from 'types/Store';
import type { HotelsFilter } from 'types/State';

export const reset = (payload: $Shape<HotelsFilter>): Action => ({ type: RESET_HOTELS_FILTER, payload }); // eslint-disable-line
export const update = (payload: $Shape<HotelsFilter>): Action => ({ type: UPDATE_HOTELS_FILTER, payload }); // eslint-disable-line
