// @flow

import uniqueId from 'lodash/uniqueId';
import {
  RESET_ERRORS,
  UPDATE_ERRORS,
  ADD_ERRORS,
  REMOVE_ITEM_ERRORS
} from 'constants/errors';

import type { Dispatch, Action } from 'types/Store';
import type { ErrorMsgs, ErrorMsg } from 'types/State';

export const reset = (payload: ErrorMsgs): Action => ({ type: RESET_ERRORS, payload });
export const update = (payload: ErrorMsg | ErrorMsg): Action => ({ type: UPDATE_ERRORS, payload });
export const add = (payload: ErrorMsg): Action => ({ type: ADD_ERRORS, payload });
export const removeItem = (payload: ErrorMsg): Action => ({ type: REMOVE_ITEM_ERRORS, payload });

export const push = (payload: ErrorMsg) => {
  const id: string = uniqueId();
  const active: boolean = true;
  return async (dispatch: Dispatch) => {
    dispatch(add({ ...payload, id, active }));
  };
};
