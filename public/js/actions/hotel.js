// @flow

import {
  RESET_HOTEL,
  UPDATE_HOTEL
} from 'constants/hotel';

import RestProvider from 'providers/Rest';
import * as hotelsActions from 'actions/hotels';
import * as errorsActions from 'actions/errors';
import config from 'config';

import type { Dispatch, Action } from 'types/Store';
import type { Hotel } from 'types/State';

const path = config.api.hotels.path;
const provider = new RestProvider({ path });

declare var _t: (str: string) => string;

export const reset = (payload: ?$Shape<Hotel>): Action => ({ type: RESET_HOTEL, payload });
export const update = (payload: $Shape<Hotel>): Action => ({ type: UPDATE_HOTEL, payload });

export const fetch = (params: { id: number }) => {
  return async (dispatch: Dispatch) => {
    dispatch(update({ isFetching: false }));
    try {
      dispatch(update({ isFetching: true }));
      const { status, data } = await provider.fetch(params);
      if (status === 200) dispatch(reset(data));
      dispatch(update({ isFetching: false }));
      return data;
    } catch (e) {
      console.error(e);
      dispatch(errorsActions.push({ msg: _t('Server sync error') }));
      dispatch(update({ isFetching: false }));
      return null;
    }
  };
};

export const remove = (params: $Shape<Hotel>) => {
  return async (dispatch: Dispatch) => {
    dispatch(hotelsActions.removeItem({ ...params, id: params.id }));
    try {
      await provider.remove(params);
    } catch (e) {
      console.error(e);
      dispatch(errorsActions.push({ msg: _t('Server sync error') }));
    }
  };
};

export const clean = (params: Object) => {
  return async (dispatch: Dispatch) => {
    try {
      const { status, data } = await provider.api({
        url: `${path}/${params.id}/clean`,
        method: 'PUT',
        data: { bedNameConfirmation: params.bedNameConfirmation }
      });

      if (status === 200) {
        dispatch(update({ ...data, errors: {} }));
        dispatch(hotelsActions.update(data));
        return data;
      }

      return null;
    } catch (e) {
      console.error(e);
      dispatch(errorsActions.push({ msg: _t('Server sync error') }));
      dispatch(update({ isFetching: false }));
      return null;
    }
  };
};

export const save = (params: $Shape<Hotel>) => {
  return async (dispatch: Dispatch) => {
    try {
      const { status, data } = await provider.save(params);

      if (status === 201) {
        dispatch(update({ ...data, errors: {} }));
        dispatch(hotelsActions.add(data));
        return data;
      }

      if (status === 200) {
        dispatch(update({ ...data, errors: {} }));
        dispatch(hotelsActions.update(data));
        return data;
      }

      if (status === 422) {
        dispatch(update({ ...params, errors: data.errors, isFetching: false }));
        return { errors: data.errors };
      }
      return null;
    } catch (e) {
      console.error(e);
      dispatch(errorsActions.push({ msg: _t('Server sync error') }));
      dispatch(update({ ...params, isFetching: false }));
      return null;
    }
  };
};
