import React from 'react';
import PropTypes from 'prop-types';
import { Router, Route, browserHistory, IndexRoute } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';

import AppPage from 'containers/AppPage';
import HotelsPage from 'containers/HotelsPage';
import HotelDetailPage from 'containers/HotelDetailPage';

const getIndexRoot = (name) => {
  switch (name) {
    case 'hotel':
      return HotelsPage;
    default:
      return HotelsPage;
  }
};

export default function Root(props) {
  const store = props.store;
  const history = syncHistoryWithStore(browserHistory, store);

  return (
    <Router history={history} key={Math.random()}>
      <Route path="/" component={AppPage}>
        <IndexRoute component={getIndexRoot(props.index)} />
        <Route path="/hotels" component={HotelsPage} />
        <Route path="/hotels/:id" component={HotelDetailPage} />
      </Route>
    </Router>
  );
}

Root.propTypes = {
  index: PropTypes.string,
  store: PropTypes.any
};

Root.defaultProps = {
  index: 'main-page'
};
