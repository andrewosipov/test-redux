import data from 'data.json';

export default class Rest {
  constructor(options = {}) {
    this.path = options.path;
  }

  fetch(params = {}) {
    return params.id ? this.fetchById(params) : this.fetchAll(params);
  }

  fetchById(params = {}) {
    return this.fetchAll.data ? this.fetchAll.data.find(item => item.id === params.id) : {};
  }

  fetchAll() {
    return {
      status: 200,
      data: data.hotels.map((hotel, index) => (
        {
          ...hotel,
          id: index + 1,
          img: `${hotel.img}&rand=${Math.random()}`
        }
      ))
    };
  }
}
