import { getToken, getCookie, getFormData, getBusinessId } from 'utils';
import config from 'config';

const apikey = getCookie('apikey');
const token = getToken();
let requests = [];

export function abort(rid) {
  const data = requests.reduce((container, item) => {
    if (item.rid === rid || !rid) {
      item.xhr.abort();
      item.xhr = null;
      return container;
    }
    container.push(item);
    return container;
  }, []);
  requests = data;
}

export function upload(params) {
  let url;
  let formData;
  const request = requests.find(item => item.rid === params.rid);

  const id = params.id;
  const blob = params.blob;
  const path = params.path;
  const name = params.name;
  if (id) {
    url = `${config.hostname}${path}/${id}`;
  } else {
    url = `${config.hostname}${path}`;
  }
  const xhr = new XMLHttpRequest();

  if (request) {
    request.xhr.abort();
  } else {
    const rid = params.rid;
    requests.push({ rid, xhr });
  }

  if (name) {
    const data = {};
    data[name] = blob;
    formData = getFormData(data);
  } else {
    formData = getFormData({ image: blob });
  }


  const businessId = getBusinessId();
  setTimeout(() => {
    xhr.open('POST', url, true);
    xhr.setRequestHeader('X-Business-Id', businessId);
    xhr.setRequestHeader('Authorization', token);
    xhr.setRequestHeader('X-Api-Key', apikey);
    xhr.send(formData);
  }, 10);
  return xhr;
}

export default {
  upload,
  abort
};
