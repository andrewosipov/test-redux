import configureStore from 'store/configureStore';
import each from 'lodash/each';
import get from 'lodash/get';
import config from 'config';
import moment from 'moment';
import XLSX from 'xlsx';

const a = document.createElement('a');
document.body.appendChild(a);
a.style = 'display: none';

const store = configureStore();


export function _t(text) { // eslint-disable-line
  return text;
}

export function getToken() {
  return `Basic ${config.apiToken}`;
}

export function getCookie(name) {
  const matches = document.cookie.match(new RegExp(
    `(?:^|; )${name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1')}=([^;]*)` //eslint-disable-line
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

export function getParameterByName(name, url) {
    if (!url) url = window.location.href; //eslint-disable-line
    name = name.replace(/[\[\]]/g, "\\$&"); //eslint-disable-line
    const regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"), //eslint-disable-line
        results = regex.exec(url); //eslint-disable-line
    if (!results) return null; //eslint-disable-line
    if (!results[2]) return ''; //eslint-disable-line
    return decodeURIComponent(results[2].replace(/\+/g, " ")); //eslint-disable-line
}

export function urlB64ToUint8Array(base64String) {
  const padding = '='.repeat((4 - base64String.length % 4) % 4); //eslint-disable-line
  const base64 = (base64String + padding)
    .replace(/\-/g, '+') //eslint-disable-line
    .replace(/_/g, '/');

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) { //eslint-disable-line
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}

export function setCookie(name, value, options) {
  options = options || {};

  let expires = options.expires;

  if (typeof expires === 'number' && expires) {
    const d = new Date();
    d.setTime(d.getTime() + (expires * 1000));

    expires = options.expires = d;
  }
  if (expires && expires.toUTCString) {
    options.expires = expires.toUTCString();
  }

  value = encodeURIComponent(value);

  let updatedCookie = `${name}=${value}`;

  Object.keys(options).forEach((item) => {
    updatedCookie += `; ${item}`;
    const propValue = options[item];
    if (propValue !== true) {
      updatedCookie += `= ${propValue}`;
    }
  });

  document.cookie = updatedCookie;
}

export function deleteCookieByName(name) {
  setCookie(name, '', {
    expires: -1
  });
}

export function getFormData(params) {
  const formData = new FormData();
  each(params, (value, key) => {
    formData.append(key, value);
  });
  return formData;
}


export function getTimeFromMins(mins) {
  const h = mins / 60 | 0;//eslint-disable-line
  const m = mins % 60 | 0;//eslint-disable-line
  return moment.utc().hours(h).minutes(m).format('HH:mm');
}

export function getStore() {
  return store;
}

export function getBusinessId() {
  const state = store.getState();
  return get(state, 'business.id');
}

export function s2ab(s) {
  const buf = new ArrayBuffer(s.length);
  const view = new Uint8Array(buf);
  for (let i = 0; i !== s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF; //eslint-disable-line
  return buf;
}

export function jsonToXlsx(params) {
  const wsName = 'User Report';
  const ws = XLSX.utils.json_to_sheet(params.list, { header: params.header });
  const colsSize = params.header.map(() => ({ wpx: 200 }));

  ws['!cols'] = colsSize;

  const wb = { SheetNames: [], Sheets: {} };

  wb.Props = { Title: 'Compass', Author: 'Compass' };

  XLSX.utils.book_append_sheet(wb, ws, wsName);

  const wopts = { bookType: 'xlsx', bookSST: false, type: 'binary' };
  const wbout = XLSX.write(wb, wopts);
  const blob = new Blob([s2ab(wbout)], { type: 'application/octet-stream' });

  return blob;
}

export function saveFile(blob, fileName) {
  const url = window.URL.createObjectURL(blob);
  a.href = url;
  a.download = fileName;
  a.click();
  window.URL.revokeObjectURL(url);
}
