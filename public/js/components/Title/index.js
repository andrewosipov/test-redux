import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import styles from './styles.css';

const cx = classNames.bind(styles);

export default function Title(props) {
  const mods = cx(...props.mods) || cx('common');
  return (
    <div className={mods} data-test-id={props.testId}>
      <div className={cx('text')}>{props.children}</div>
    </div>
  );
}

Title.defaultProps = {
  mods: ['common'],
  testId: 'title'
};

Title.propTypes = {
  mods: PropTypes.array,
  testId: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
    PropTypes.node,
    PropTypes.element
  ])
};
