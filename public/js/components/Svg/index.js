import React from 'react';
import svg from '../../svgInit';
import styles from './styles.css';

function getIcons() {
  return svg.map((item) => {
    return (
      <symbol
        data-test-id="SVG"
        id={`icon-${item.name}`}
        viewBox="0 0 128 128"
        key={`item-${item.name}`}
        dangerouslySetInnerHTML={{ __html: item.svg }}
      />
    );
  });
}

function Svg() {
  return (
    <div className={styles.common}>
      <svg xmlns="http://www.w3.org/2000/svg">
        {getIcons()}
      </svg>
    </div>
  );
}

export default Svg;
