// @flow

import React, { Component } from 'react';
import classNames from 'classnames/bind';
import styles from './styles.css';

const cx = classNames.bind(styles);

type Props = {
  mods: Array<{ [key: $Keys<typeof styles>]: boolean } | $Keys<typeof styles>>,
  el: ?Object,
  testId: string,
  pattern: ?string,
  type: ?string,
  name: ?string,
  placeholder: ?string,
  value: ?string,
  disabled: ?boolean,
  autoComplete: ?boolean,
  el: ?Function,
  onClick: ?Function,
  onChange: ?Function,
  onKeyDown: ?Function,
  onKeyPress: ?Function,
  onKeyUp: ?Function,
  onFocus: ?Function,
  onBlur: ?Function
}

export default class Input extends Component<Props> {
  defaultProps: Object;
  input: Object;

  render() {
    const mods = cx(...this.props.mods) || cx('common');
    return (
      <div
        className={mods}
        data-test-id={this.props.testId}
      >
        <input
          pattern={this.props.pattern}
          type={this.props.type}
          name={this.props.name}
          placeholder={this.props.placeholder}
          value={this.props.value}
          disabled={this.props.disabled}
          className={cx('input')}
          onChange={this.props.onChange}
          onClick={this.props.onClick}
          onKeyDown={this.props.onKeyDown}
          onKeyPress={this.props.onKeyPress}
          onKeyUp={this.props.onKeyUp}
          onFocus={this.props.onFocus}
          onBlur={this.props.onBlur}
          autoComplete={this.props.autoComplete}
          ref={this.getEl}
        />
      </div>
    );
  }

  getEl: Function = (el: Object) => {
    this.input = el;
    const tmpStr = this.input && this.input.value;
    this.props.el && this.props.el(el);
    this.input && this.input.focus();
    this.input && (this.input.value = '');
    this.input && (this.input.value = tmpStr);
  }

  static defaultProps = {
    mods: ['common'],
    label: '',
    name: '',
    placeholder: '',
    icon: null,
    type: '',
    value: '',
    pattern: '',
    iconMods: ['common'],
    disabled: null,
    el: null,
    autoComplete: null,
    testId: 'input',
    onClick: null,
    onChange: null,
    onKeyPress: null,
    onKeyDown: null,
    onKeyUp: null,
    onFocus: null,
    onBlur: null
  }
}
