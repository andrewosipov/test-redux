import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import styles from './styles.css';

const cx = classNames.bind(styles);

export default function Layout(props) {
  const mods = cx(...props.mods) || cx('common');

  return (
    <div
      className={mods}
      data-test-id={props.testId}
    >
      <div className={cx('content')}>
        {props.children}
      </div>
    </div>
  );
}


Layout.defaultProps = {
  mods: ['common'],
  testId: 'layout',
  children: ''
};

Layout.propTypes = {
  mods: PropTypes.array,
  children: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
    PropTypes.node,
    PropTypes.element
  ]),
  testId: PropTypes.string
};
