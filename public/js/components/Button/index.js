// @flow

import React from 'react';
import classNames from 'classnames/bind';
import Icon from 'components/Icon';
import styles from './styles.css';

const cx = classNames.bind(styles);

type Props = {
  mods: Array<{ [key: $Keys<typeof styles>]: boolean } | $Keys<typeof styles>>,
  icon: ?string | ?Object,
  label: string,
  testId: string,
  disabled: boolean,
  target: ?string,
  href: ?string,
  type: ?string,
  onClick: ?Function,
  children: ?any
}

export default function Button(props: Props) {
  const mods = cx(...props.mods) || cx('common');
  const Tag = props.href ? 'a' : 'button';
  return (
    <Tag
      data-test-id={props.testId}
      className={mods}
      disabled={props.disabled}
      onClick={props.onClick}
      href={props.href}
      target={props.target}
      type={props.type}
    >
      {props.icon && props.icon}
      <div className={cx('title')}>{props.children || props.label}</div>
      <div className={cx('loader')}>
        <Icon name="loader" mods={['small', 'spinner']} />
      </div>
    </Tag>
  );
}

Button.defaultProps = {
  mods: ['common'],
  onClick: null,
  testId: 'button',
  disabled: false,
  target: null,
  icon: null,
  href: null,
  type: null,
  min: null,
  max: null,
  pattern: null,
  step: null,
  required: false,
  children: null
};
