// @flow

import React, { Component } from 'react';

import range from 'lodash/range';
import classNames from 'classnames/bind';

import Icon from 'components/Icon';
import Box from 'components/Box';
import Title from 'components/Title';
import Input from 'components/Input';
import CheckList from 'components/CheckList';

import type { HotelsFilter, Base } from 'types/State';

import * as baseActions from 'actions/base';
import * as hotelsFilterActions from 'actions/hotels-filter';

import styles from './styles.css';

const cx = classNames.bind(styles);

declare var _t: (str: string) => string;

type Props = {
  mods: Array<{ [key: $Keys<typeof styles>]: boolean } | $Keys<typeof styles>>,
  items: Array<any>,
  testId: string,
  baseState: Base,
  hotelsFilterState: HotelsFilter,
  baseActions: typeof baseActions,
  hotelsFilterActions: typeof hotelsFilterActions
};

type State = {
  hotelName: ?string,
  rate: number,
  freebies: Array<string>
}

export default class Filter extends Component<Props, State> {
  searchDelay: any;

  state = {
    hotelName: '',
    rate: 0,
    freebies: []
  };

  stars(index: number) {
    return (
      <Box key={index} mods={['indent-bottom-3', 'column']}>
        <Title mods={['small']}>{_t('Stars')}</Title>
        <Box mods={['indent-top-2']}>
          {
            range(0, 5).map((ind) => {
              const active = ind === 0 || ind <= this.state.rate - 1 ? 'blue' : '';
              const text = ind === 0 ? '0+' : ind + 1;
              const rate = ind === 0 ? 0 : ind + 1;
              return (
                <Icon
                  key={ind}
                  name="star2"
                  text={text}
                  mods={['big', 'indent', active]}
                  onClick={() => this.filterByRate(rate)}
                />
              );
            })
          }
        </Box>
      </Box>
    );
  }

  freebies(index: number) {
    return (
      <Box key={index} mods={['indent-bottom-3', 'column']}>
        <Title mods={['small']}>{_t('Freebies')}</Title>
        <CheckList
          selected={this.props.hotelsFilterState.freebies}
          onChange={freebies => this.filterByFreebies(freebies)}
          items={[
            { value: 'breakfast', name: 'Free Breakfast' },
            { value: 'parking', name: 'Free Parking' },
            { value: 'cancellation', name: 'Free Cancellation' },
            { value: 'shattle', name: 'Free Airport Shattle' }
          ]}
        />
      </Box>
    );
  }

  hotelName(index: number) {
    const value = this.props.hotelsFilterState && !this.props.baseState.isFetching ?
      this.props.hotelsFilterState.hotelName : this.state.hotelName;
    return (
      <Box key={index} mods={['indent-bottom-3', 'column']}>
        <Title mods={['small']}>{_t('Hotel Name')}</Title>
        <Box mods={['indent-top-1']}>
          <Input
            mods={['full', 'search']}
            value={value}
            placeholder={_t('Hotel Name')}
            onChange={el => this.filterByHotelName(el.target.value)}
          />
        </Box>
      </Box>
    );
  }

  filterByFreebies(freebies: Array<string> = []) {
    this.setState({ freebies });
    this.searchHandler(() => {
      this.props.hotelsFilterActions.update({ freebies });
    });
  }

  filterByRate(rate: number = 0) {
    this.setState({ rate });
    this.searchHandler(() => {
      this.props.hotelsFilterActions.update({ rate });
    });
  }

  filterByHotelName(hotelName: string = '') {
    this.setState({ hotelName });
    this.searchHandler(() => {
      this.props.hotelsFilterActions.update({ hotelName });
    });
  }

  async searchHandler(callback: Function) {
    clearTimeout(this.searchDelay);
    this.props.baseActions.update({ isFetching: true });
    this.searchDelay = setTimeout(async () => {
      callback();
      this.props.baseActions.update({ isFetching: false });
    }, 600);
  }

  render() {
    const mods = cx(...this.props.mods) || cx('common');
    return (
      <div
        className={mods}
        data-test-id={this.props.testId}
      >
        {this.props.items.map((option, index) => this[option](index))}
      </div>
    );
  }

  static defaultProps = {
    mods: ['common'],
    items: [],
    testId: 'accordion',
    getContent: () => (<div>test</div>),
    getTitle: () => null,
    getSubtitle: () => null,
    getStatus: () => null
  }
}
