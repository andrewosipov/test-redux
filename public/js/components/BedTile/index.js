// @flow

import React from 'react';
import classNames from 'classnames/bind';
import Scroller from 'components/Scroller';

import styles from './styles.css';

const cx = classNames.bind(styles);

declare var _t: (str: string) => string;

type Props = {
  mods: Array<{ [key: $Keys<typeof styles>]: boolean } | $Keys<typeof styles>>,
  title: string,
  datetime: string,
  onClick: ?Function,
  testId: string
}

export default function BedTile(props: Props) {
  const mods = cx(...props.mods) || cx('common');
  const datetime = props.datetime;

  return (
    <div className={mods} data-test-id={props.testId}>
      <div className={cx('content')} data-test-id="content" onClick={props.onClick}>
        <Scroller isCenter>
          {props.title}
        </Scroller>
        {datetime && <div className={cx('datetime')}>{datetime}</div>}
      </div>
    </div>
  );
}

BedTile.defaultProps = {
  mods: ['common'],
  testId: 'title',
  title: '17',
  datetime: '',
  onClick: null
};
