import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';

import styles from './styles.css';

const cx = classNames.bind(styles);

export default function Loader(props) {
  const mods = cx(...props.mods) || cx('common');

  return (
    <div className={mods} data-test-id={props.testId}>
      <div className={cx('loader')} />
    </div>
  );
}

Loader.defaultProps = {
  mods: ['common'],
  testId: 'loader'
};

Loader.propTypes = {
  mods: PropTypes.array,
  testId: PropTypes.string
};
