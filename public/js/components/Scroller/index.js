// @flow
import React, { Component } from 'react';
import type { Element, ChildrenArray } from 'react';
import classNames from 'classnames/bind';
import styles from './styles.css';

const cx = classNames.bind(styles);

type Props = {
  mods: Array<{ [key: $Keys<typeof styles>]: boolean } | $Keys<typeof styles>>,
  isScrollable: boolean,
  isShadowLeft: boolean,
  isShadowRight: boolean,
  children: Element<any> | ChildrenArray<Element<any>> | string,
  testId: string,
  isCenter: boolean
}

type State = {
  isScrollable: boolean,
  isShadowLeft: boolean,
  isShadowRight: boolean,
  isNotScroll: boolean,
  scrollWidth: number,
  clientWidth: number
}

export default class Scroller extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      isScrollable: props.isScrollable,
      isShadowLeft: props.isShadowLeft,
      isShadowRight: props.isShadowRight,
      scrollWidth: 0,
      clientWidth: 0,
      isNotScroll: false
    };
  }

  render() {
    const { isShadowLeft, isShadowRight, isNotScroll } = this.state;
    const { isCenter } = this.props;

    const shadowMod = {
      'shadow-left': isShadowLeft,
      'shadow-right': isShadowRight,
      center: !!(isCenter && isNotScroll)
    };

    const mods = cx(...this.props.mods, shadowMod) || cx('common', shadowMod);

    return (
      <div className={mods} data-test-id={this.props.testId}>
        <div
          className={cx('container')}
          ref={this.handleRef}
          onScroll={this.handleScroll}
        >
          {this.props.children}
        </div>
      </div>
    );
  }

  handleScroll = (e: SyntheticEvent<HTMLDivElement>) => {
    const { clientWidth } = this.state;
    const scrollLeft = e.currentTarget.scrollLeft;
    const scrollWidth = e.currentTarget.scrollWidth;
    const offsetScroll = scrollLeft + clientWidth + 2;
    const isShadowRight = (scrollWidth >= offsetScroll);
    const isShadowLeft = (scrollLeft > 0);
    this.setState({ isShadowRight, isShadowLeft });
  }

  handleRef = (el: ?HTMLElement) => {
    if (el) {
      const { scrollWidth, clientWidth } = el;
      const isNotScroll = scrollWidth <= clientWidth;
      const isReverse = this.props.mods.some(mod => mod === 'reverse');
      const isShadowRight = !isReverse && (scrollWidth > clientWidth);
      const isShadowLeft = isReverse && (scrollWidth > clientWidth);
      this.setState({ scrollWidth, clientWidth, isShadowRight, isShadowLeft, isNotScroll });
    }
  }

  static defaultProps = {
    mods: ['common'],
    testId: 'Scroller',
    isScrollable: false,
    isShadowLeft: false,
    isShadowRight: false,
    isCenter: false
  };
}
