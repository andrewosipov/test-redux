// @flow

import React from 'react';
import classNames from 'classnames/bind';
import range from 'lodash/range';
import Icon from 'components/Icon';

import styles from './styles.css';

const cx = classNames.bind(styles);

type Props = {
  mods: Array<{ [key: $Keys<typeof styles>]: boolean } | $Keys<typeof styles>>,
  rate: number,
  testId: string
}

export default function Raiting(props: Props) {
  const mods = cx(...props.mods) || cx('common');
  const rate = props.rate >= 1 && props.rate <= 5 ? props.rate : 3;

  return (
    <div className={mods} data-test-id={props.testId}>
      {range(0, rate).map(index => <Icon key={index} name="star" mods={['small', 'indent']} />)}
    </div>
  );
}

Raiting.defaultProps = {
  mods: ['common'],
  testId: 'rate',
  rate: 0
};
