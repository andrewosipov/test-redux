// @flow

import React from 'react';
import classNames from 'classnames/bind';
import styles from './styles.css';

const cx = classNames.bind(styles);

type Props = {
  mods: Array<{ [key: $Keys<typeof styles>]: boolean } | $Keys<typeof styles>>,
  title: string,
  testId: string
}

export default function Tag(props: Props) {
  const mods = cx(...props.mods) || cx('common');

  return (
    <div className={mods} data-test-id={props.testId}>
      <div className={cx('rect')} />
      <div className={cx('title')}>{props.title}</div>
    </div>
  );
}


Tag.defaultProps = {
  mods: ['common'],
  testId: 'tag'
};

