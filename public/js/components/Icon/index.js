// @flow

import React from 'react';
import classNames from 'classnames/bind';

import styles from './styles.css';

const cx = classNames.bind(styles);

type Props = {
  mods: Array<{ [key: $Keys<typeof styles>]: boolean } | $Keys<typeof styles>>,
  title: string | ?number,
  name: string,
  text: ?string | ?number,
  testId: string,
  onClick: ?Function
};

export default function Icon(props: Props) {
  const mods = cx(...props.mods) || cx('common');
  return (
    <div
      className={mods}
      title={props.title}
      onClick={props.onClick}
      data-test-id={props.testId}
    >
      <svg
        className={styles.svg}
        dangerouslySetInnerHTML = {{ // eslint-disable-line
          __html: `
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-${props.name}"></use>
          `
        }}
      />
      <span className={cx('text')}>{props.text}</span>
    </div>
  );
}

Icon.defaultProps = {
  mods: ['common'],
  title: '',
  text: '',
  onClick: null,
  testId: 'icon'
};
