// @flow

import React from 'react';
import classNames from 'classnames/bind';
import styles from './styles.css';

const cx = classNames.bind(styles);

type Props = {
  mods: Array<{ [key: $Keys<typeof styles>]: boolean } | $Keys<typeof styles>>,
  id: number,
  name: string,
  label: string,
  testId: string,
  disabled: boolean,
  checked: boolean,
  onChange: ?Function
}

export default function Checkbox(props: Props) {
  const mods = cx(...props.mods) || cx('common');

  return (
    <div className={mods} data-test-id={props.testId}>
      <input
        className={cx('checkbox')}
        id={props.id}
        name={props.name}
        type="checkbox"
        disabled={props.disabled}
        onChange={props.onChange && props.onChange}
        checked={props.checked}
      />
      <label htmlFor={props.id} className={cx('label')} >{props.label}</label>
    </div>
  );
}

Checkbox.defaultProps = {
  id: '',
  testId: 'checkbox',
  mods: ['common'],
  disabled: false,
  onChange: () => null
};
