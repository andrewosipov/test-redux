// @flow

import React from 'react';
import classNames from 'classnames/bind';
import styles from './styles.css';

const cx = classNames.bind(styles);

type Props = {
  mods: Array<{ [key: $Keys<typeof styles>]: boolean } | $Keys<typeof styles>>,
  onClick: ?Function,
  testId: string,
  children: any
}

export default function Box(props: Props) {
  const mods = cx(...props.mods) || cx('common');

  return (
    <div
      data-test-id={props.testId}
      className={mods}
      onClick={props.onClick}
    >
      {props.children}
    </div>
  );
}

Box.defaultProps = {
  mods: ['common'],
  testId: 'box',
  onClick: null
};
