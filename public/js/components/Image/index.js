// @flow

import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import styles from './styles.css';

const cx = classNames.bind(styles);

type Props = {
  mods: Array<{ [key: $Keys<typeof styles>]: boolean } | $Keys<typeof styles>>,
  src: string,
  testId: string,
  onClick: ?Function
};

export default function Image(props: Props) {
  const mods = cx(...props.mods) || cx('common');
  const style = {
    backgroundImage: `url(${props.src})`
  };
  return (
    <div
      style={props.src ? style : {}}
      data-test-id={props.testId}
      className={mods}
      onClick={props.onClick}
    />
  );
}

Image.defaultProps = {
  mods: ['common'],
  testId: 'box',
  src: null
};

Image.propTypes = {
  mods: PropTypes.array,
  testId: PropTypes.string,
  src: PropTypes.string,
};
