// @flow

import React, { Component } from 'react';
import classNames from 'classnames/bind';
import Icon from 'components/Icon';
import Checkbox from 'components/Checkbox';
import styles from './styles.css';

const cx = classNames.bind(styles);

type Props = {
  mods: Array<any>,
  items: Array<Object>,
  selected: Array<string | number>,
  onChange?: (selected: Array<any>) => void,
  getIconName: (params: Object) => string | number,
  getName: (params: Object) => string | number,
  getValue: (params: Object) => string | number,
  isCrossed: boolean,
  testId: string,
  onChange: ?Function
}

type State = {
  selected: Array<string | number> | null,
}

export default class CheckList extends Component<Props, State> {
  state = {
    selected: this.props.selected
  }

  render() {
    const mods = cx(...this.props.mods) || cx('common');
    const { getName, getValue, isCrossed, getIconName } = this.props;
    return (
      <div className={mods} data-test-id={this.props.testId}>
        {
          this.props.items.map((item, index) => {
            const isChecked = this.state.selected.some(id => id === item.value);
            return (
              <Checkbox
                key={item.value}
                id={item.value}
                mods={[]}
                name={item.name}
                label={item.name}
                checked={isChecked}
                onChange={() => this.handleItemClick(item)}
              />
            );
          })
        }
      </div>
    );
  }

  handleItemClick = (item: Object) => {
    const prevSelected = this.state.selected;
    const value = item.value;
    const hasSomeSelected = prevSelected.some(i => i === value);
    let selected;

    if (hasSomeSelected) {
      selected = prevSelected.filter(i => i !== value);
    } else {
      selected = [...prevSelected, value];
    }

    this.setState({ selected });
    this.props.onChange(selected);
  }

  static defaultProps = {
    getIconName: (item: Object) => item.icon,
    getName: (item: Object) => item.name,
    getValue: (item: Object) => item.id,
    testId: 'check-list',
    onChange: null,
    mods: ['common'],
    selected: [],
    items: [],
    isCrossed: true
  }
}
