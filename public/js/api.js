// @flow

import axios from 'axios';
import get from 'lodash/get';
import set from 'lodash/set';
import config from 'config';
import { getCookie, setCookie } from 'utils';

const getErrors = (errors) => {
  if (errors && typeof errors === 'object' && Array.isArray(errors)) {
    return errors.reduce((box, item) => (
      item.propertyPath ?
        { ...box, [item.propertyPath]: item.message } :
        { ...box, base: [...box.base, item.message] }
    ), { base: [] });
  }
  return null;
};

const headers = {
  Authorization: `Basic ${config.apiToken}`,
  Accept: 'application/json',
  'Content-type': 'application/json'
};

const baseURL = config.hostname;
const validateStatus = status => ((status === 422 || (status >= 200 && status < 300)));

const api = axios.create({ baseURL, headers, validateStatus });

api.interceptors.request.use((params) => {
  const token = getCookie('token');
  if (token != null) {
    set(params, 'headers.common[X-Token]', token);
  }
  return params;
}, (error) => {
  return Promise.reject(error);
});

const getRefreshToken = async () => {
  const method = 'POST';
  const refreshToken = getCookie('refresh_token');
  const data = { refresh_token: refreshToken };
  const url = `${config.hostname}${config.api.refreshToken.path}`;
  const response = await axios({ headers, url, method, data });
  return response.data;
};

const setRefreshToken = async (request) => {
  try {
    const data = await getRefreshToken();
    if (data.token && data.refresh_token) {
      const token = data.token;
      setCookie('token', data.token, { path: '/', expires: 3600 });
      setCookie('refresh_token', data.refresh_token, { path: '/', expires: 3600 * 24 * 30 });
      set(request, 'headers[X-Token]', token);
      return axios(request);
    }
  } catch (e) {
    window.location = '/auth';
  }
  return null;
};

const tokenRefreshInterceptor = async (error) => {
  const originalRequest = error.config;
  const isRetry = get(originalRequest, '_retry');

  if (error.response && error.response.status === 401 && !isRetry) {
    set(originalRequest, '_retry', true);
    return setRefreshToken(originalRequest);
  }

  return Promise.reject(error);
};

api.interceptors.response.use(response => response, tokenRefreshInterceptor);

api.interceptors.response.use((response) => {
  const { status, data } = response;
  if (status === 422 && data && typeof data === 'object') {
    const errors = getErrors(data.violations) || {};
    set(response, 'data.errors', errors);
    return response;
  }
  return response;
}, (error) => {
  return Promise.reject(error.response);
});

export default api;
