import 'regenerator-runtime/runtime'; //eslint-disable-line
import React from 'react';
import 'api';

import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { AppContainer } from 'react-hot-loader'; //eslint-disable-line

import normalize from 'normalize.css'; //eslint-disable-line
import styles from 'utils/styles/main.css'; //eslint-disable-line

import { _t, getStore } from 'utils';


import Root from './Root';

window._t = _t; //eslint-disable-line

const store = getStore();

(async () => {
  const userData = true;
  if (userData) {
    ReactDOM.render(
      <AppContainer>
        <Provider store={store}>
          <Root store={store} index="main-page" />
        </Provider>
      </AppContainer>,
      document.getElementById('root')
    );

    // Hot Module Replacement API
    if (module.hot) {
      module.hot.accept('./Root', () => {
        const NextRoot = require('./Root').default; //eslint-disable-line
        ReactDOM.render(
          <AppContainer>
            <Provider store={store}>
              <NextRoot store={store} />
            </Provider>
          </AppContainer>,
          document.getElementById('root')
        );
      });
    }
  } else {
    ReactDOM.render(
      // <Authenticated />,
      document.getElementById('root')
    );
  }
})();
