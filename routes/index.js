const express = require('express'); // eslint-disable-line
const router = express.Router(); // eslint-disable-line

const isProduction = process.env.NODE_ENV === 'production';

/* GET home page. */
router.get('/', (req, res) => {
  res.render('index', { title: process.env.TITLE, isProduction, timestamp: new Date().getTime() });
});

module.exports = router;
