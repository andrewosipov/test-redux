var express = require('express');
var router = express.Router();
var isProduction = process.env.NODE_ENV === 'production';

/* GET users listing. */
router.get('/', function(req, res, next) {
  var apikey = req.query.key;
  res.cookie('apikey', apikey);
  res.render('index', { title: process.env.TITLE, isProduction: isProduction, timestamp: new Date().getTime() });
});

module.exports = router;
